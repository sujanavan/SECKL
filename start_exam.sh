#!/bin/sh

#For xmodmap keycodes refer the xmodmap directory

#The following line represents a Escape Keycode set to F18 which does not exist

#xmodmap -e "keycode 9=F18"
#xmodmap -e "keycode <<CODE>>=F18"

#Start keycode mapping

#xmodmap -e "keycode 9=F18"	#Escape
#xmodmap -e "keycode 23=F18"	#TabSpace
#xmodmap -e "keycode 37=F18"	#Left Control
#xmodmap -e "keycode 105=F18"	#Right Control
#xmodmap -e "keycode 64=F18"	#Left ALT
#xmodmap -e "keycode 108=F18"	#Right ALT
#xmodmap -e "keycode 67=F18"	#F1
#xmodmap -e "keycode 68=F18"	#F2
#xmodmap -e "keycode 69=F18"	#F3
#xmodmap -e "keycode 70=F18"	#F4
#xmodmap -e "keycode 71=F18"	#F5
#xmodmap -e "keycode 72=F18"	#F6
#xmodmap -e "keycode 73=F18"	#F7
#xmodmap -e "keycode 74=F18"	#F8
#xmodmap -e "keycode 75=F18"	#F9
#xmodmap -e "keycode 76=F18"	#F10
#xmodmap -e "keycode 95=F18"	#F11
#xmodmap -e "keycode 96=F18"	#F12
#xmodmap -e "keycode 92=F18"	#Shift

#Following is a Loop to handle 132-255 for all special functions

#keycode=132
#while [ $keycode -le 255 ]
#do
#xmodmap -e "keycode $keycode=F18"
#keycode=$((keycode+1))
#done

#End keycode mapping 

chromium-browser --kiosk http://www.google.com
