# SECKL
Secure Exam in Chromium Kiosk for Linux (Alternative to Safe Exam Browser for Windows &amp; Mac)

Secure Exam in Chromium Kiosk for Linux requires chromium browser version 45+ (KIOSK Enabled Versions Only)

To know about the xmodmap keycodes refer the README in the xmodmap directory

Only keycodes to disable and URL are to be included in the "start_exam.sh" shell script

Note: Keycodes vary with platform and have to be generated again with change in platform

Provide execution permissions for "start_exam.sh"

Zip the file to avoid change of permissions

Extract it on other systems and Execute

To encrypt the shell script you might need - "Shell Script Compiler" - https://github.com/neurobin/shc
